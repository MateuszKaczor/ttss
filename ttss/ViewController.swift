//
//  ViewController.swift
//  ttss
//
//  Created by Mateusz on 05/04/16.
//  Copyright © 2016 Mateusz. All rights reserved.
//

import UIKit
//import TTSSJsonRequest

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    
    let jsonReq = TTSSAutocompletStops()
    
    
    /*
        @brief collection with TTSSStops
     
        TTSSStops represents a single tram stop
     */
    var arrayOfStops : Array<TTSSStops> = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        jsonReq.delegate = self;

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK - text field
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBAction func editingChanged(_ sender: UITextField) {
        
        if (searchTextField.text != nil) {

                jsonReq.findStops(searchTextField.text!)
        

        }
        
    }

    //MARK - TableView DataSource, Delegate
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
    {

        return arrayOfStops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        cell.textLabel!.text="\(arrayOfStops[(indexPath as NSIndexPath).row].name)"

        
        return cell
    }

}


//MARK: TTSSJsonRequest delegate
extension ViewController: TTSSAutocompletStopsDelegate {
    
    func didReciveData(_ sender: TTSSJsonRequest, data: Array<TTSSStops>){
        self.arrayOfStops = data
        tableView.reloadData()
    }
    
}


