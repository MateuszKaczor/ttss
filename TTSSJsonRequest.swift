//
//  TTSSJsonRequest.swift
//  ttss
//
//  Created by Mateusz on 05/04/16.
//  Copyright © 2016 Mateusz. All rights reserved.
//

import UIKit



class TTSSJsonRequest: NSObject {

    
    let sesion = URLSession(configuration: URLSessionConfiguration.default)
    var task: URLSessionDataTask?
    
    /*
        @brief download data from ttss.krakow.pl
     
        @param parameters It includes GET parameters required to retrieve data
    */
    func downloadJSONWitchParam(_ parameters : String){
        
        //stop task if run
        if (task != nil) {
            task?.cancel()
        }

        //add param
        let url = URL(string: "http://www.ttss.krakow.pl/internetservice/services/lookup/autocomplete/json?\(parameters)")
        
        //utf 8
        let request = NSMutableURLRequest(url: url!)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")


            task = sesion.dataTask(with: request, completionHandler: {(data, response, error) in

                
                self.convertData(data)
                
                
                
                //TODO: ERROR
            }) 

        
        task!.resume()

    }
    
    
    func convertData(_ data: Data?) {
        
        
    }
    


    
}
