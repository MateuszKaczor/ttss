//
//  TTSSStops.swift
//  ttss
//
//  Created by Mateusz on 06/04/16.
//  Copyright © 2016 Mateusz. All rights reserved.
//

import UIKit


/*
    @brief It contains information about a single stop
*/
class TTSSStops: NSObject {
    var stopId : Int = 0
    var name : String = ""
    var type : String = ""
    
    
    /*
     @brief convert dictionary data into array of TTSSStops
     
     @param dictionary example: ["id": 125, "type": stop, "name": Rondo Mogilskie]
     */
    init(dictionary : Dictionary<String, AnyObject>) {
        stopId = (dictionary["id"]?.intValue)!
        
        
        if (dictionary["name"] != nil)  {
            name = String(htmlEncodedString:  dictionary["name"] as! String)
        }
        
        if (dictionary["type"] != nil) {
            type = String(htmlEncodedString: dictionary["type"] as! String)
        }
        
    }
    
    /*
        @brief convert dictionary data into array of TTSSStops
     */
    init(name : String, type : String, stopId : Int) {
        self.stopId = stopId
        self.name = name
        self.type = type
    }
}

/*
    @brief remove HTML characters
 
    @source http://stackoverflow.com/questions/25607247/how-do-i-decode-html-entities-in-swift
*/
extension String {
    init(htmlEncodedString: String) {
        
        
        let encodedData = htmlEncodedString.data(using: String.Encoding.utf8)
        let attributedOptions : [String: AnyObject] = [
            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType as AnyObject,
            NSCharacterEncodingDocumentAttribute: String.Encoding.utf8 as AnyObject
        ]
        
        var attributedString:NSAttributedString?
        
        do{
            if (encodedData != nil) {
                attributedString = try NSAttributedString(data: encodedData!, options: attributedOptions, documentAttributes: nil)
            }
            
        }catch{
           // print("atribute string: \(error)")
        }
        
        if (attributedString != nil) {
            self.init(attributedString!.string)
        }else{
            self.init("")
        }
        
    }
}



