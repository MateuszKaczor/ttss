//
//  TTSSAutocompletStops.swift
//  ttss
//
//  Created by Mateusz on 06/04/16.
//  Copyright © 2016 Mateusz. All rights reserved.
//

import UIKit



protocol TTSSAutocompletStopsDelegate: class {
    
    /*
        @brief The method informs about the download Stops is complete
     
        @param data collection with TTSSStops
        @param sender A object requesting the cell
     */
    func didReciveData(_ sender: TTSSJsonRequest, data: Array<TTSSStops>)
}

/*
    @brief Searches stops including "phrase" string in the name
*/
class TTSSAutocompletStops: TTSSJsonRequest {
    
    
    
    
    weak var delegate:TTSSAutocompletStopsDelegate?
    
    
    /*
        @brief collection of stops
    */
    fileprivate var arrayOfStops : Array<TTSSStops> = []
    
    
    /*
        @brief Searches stops contain string "phrase"
     */
    func findStops(_ phrase : String) {
        //find
        
       // var searchPhrase = "query=\(phrase)"
        
        //remove illegal char
        let expectedCharSet = CharacterSet.urlQueryAllowed
        let searchPhrase = ("query=\(phrase)").addingPercentEncoding(withAllowedCharacters: expectedCharSet)
        
        //download stops
        if (searchPhrase != nil) {
            self.downloadJSONWitchParam(searchPhrase!)
        }
        
    }

    /*
         @brief convert json data into array of TTSSStops
         
         @data data recived from NSURLSession
    */
    override func convertData(_ data: Data?) {
        

        //remove all stops from collection
        arrayOfStops.removeAll();
        
        if (data != nil){
            do{
                let jsonResult: Array<AnyObject> = (try JSONSerialization.jsonObject(with: data!, options:
                    JSONSerialization.ReadingOptions.mutableContainers)) as! Array<AnyObject>
                

                
                for obj: AnyObject in jsonResult {
                    
                    //map object into dictionary
                    if let dict = obj as? [String: AnyObject] {
                        
                        
                        //remove "delimeter" object
                        //example: ["type": divider, "count": 2, "name": Przystanki]
                        if !(dict["count"] != nil){
                            
                           // print("STOP: \(dict)")
                            
                            //add new element
                            let stop = TTSSStops(dictionary: dict)
                            arrayOfStops.append(stop)

                            
                        }
                        
                    }
                    
                    
                }
     
                
            } catch {
                //empty
            }
        }
        
        
        // if OK
        DispatchQueue.main.async {
            //TODO: check selector
            self.delegate?.didReciveData(self, data: self.arrayOfStops)
        }
        
    }
    
    
}
